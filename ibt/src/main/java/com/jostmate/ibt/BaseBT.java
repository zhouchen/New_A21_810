package com.jostmate.ibt;

import android.content.Context;

/**
 * @author andrewliu
 * @create 2018/7/18
 * @Describe
 */
public interface BaseBT {
    void setContext(Context context);

    int openBt() throws Exception;

    int closeBt() throws Exception;

    int readBt(byte[] inputBuff) throws Exception;

    int writeBt(byte[] outputBuff, int length) throws Exception;

    boolean getIsConneted() throws Exception;
}
