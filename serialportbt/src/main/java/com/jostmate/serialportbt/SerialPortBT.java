package com.jostmate.serialportbt;

import android.content.Context;

import com.joesmate.a21.serial_port_api.libserialport_api;
import com.jostmate.ibt.BaseBT;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author andrewliu
 * @create 2018/7/18
 * @Describe
 */
public class SerialPortBT implements BaseBT {
    private String path = "";
    private int baud = 115200;
    private Context mContext;
    private int mfd = 0;

    public SerialPortBT(Context contex) {
        mContext = contex;
        try {
            iniConfig();//加载配置换文件
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void iniConfig() throws Exception {
        Properties properties = new Properties();
        InputStream _in = mContext.getAssets().open("Bt.config");//读取配置文件
        properties.load(_in);
        path = properties.getProperty("path", "");//获取串路径
        baud = Integer.parseInt(properties.getProperty("baudrate", "115200"));//获取波特率
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public int openBt() throws Exception {
        mfd = libserialport_api.device_open(path, baud);
        if (mfd > 0)
            return 0;
        else
            return -1;
    }

    @Override
    public int closeBt() throws Exception {
        if (mfd <= 0)
            return -1;
        libserialport_api.device_close(mfd);
        return 0;
    }

    @Override
    public int readBt(byte[] inputBuff) throws Exception {
        return libserialport_api.device_read_all(mfd, inputBuff);
    }

    @Override
    public int writeBt(byte[] outputBuff, int length) throws Exception {
        libserialport_api.device_bt_write(mfd, outputBuff, length);
        return 0;
    }
    @Override
    public boolean getIsConneted() throws Exception {
        if (mfd > 0)
            return true;
        else
            return false;
    }
}
