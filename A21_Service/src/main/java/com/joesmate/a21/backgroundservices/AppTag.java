package com.joesmate.a21.backgroundservices;

/**
 * Created by andre on 2017/7/15 .
 */

public class AppTag {
    public final static String TAG_BT_IN_DATA = App.getInstance().getString(R.string.bt_in_data_tag);
    public final static String TAG_BT_IN_DATA_LEN = App.getInstance().getString(R.string.bt_in_data_len_tag);
}
