package com.joesmate.a21.backgroundservices.bin;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.sdk.util.ToolFun;

import vpos.apipackage.IDCard;

/**
 * Created by andre on 2017/7/20 .
 */

public class IDCardRead {
    static final String TAG = IDCardRead.class.toString();
  //  static int m360fd = App.getInstance().m_360fd;

    App mApp = App.getInstance();
    private IDCardRead() {
    }

    private static final IDCardRead mInstance = new IDCardRead();

    public static IDCardRead getInstance() {
        IDCard.Lib_IDCardClose();//身份证下电

        ToolFun.Dalpey(200);
        IDCard.Lib_IDCardOpen();//身份证上电
        return mInstance;
    }


    public byte[] ReadBaseMsg() { //读取身份证信息不带指纹
        //读取身份证信息  不带指纹

        byte[] idcard=new byte[1297];
      int i=  IDCard.Lib_IDCardReadData(idcard,0,40);
       if(i==0){
           mApp.tts.speak("读卡成功");
           IDCard.Lib_IDCardClose();
           return idcard;
       }else {
           mApp.tts.speak("读卡失败");
           String str = String.valueOf(i);

           IDCard.Lib_IDCardClose();
           return ToolFun.hexStringToBytes(str);
       }
    }

    public byte[] ReadBaseMsgFp() { //读取身份证信息带指纹

        byte[] idcard=new byte[2321];
        int i=  IDCard.Lib_IDCardReadData(idcard,1,40);
        if(i==0){
            mApp.tts.speak("读卡成功");
            IDCard.Lib_IDCardClose();
            return idcard;
        }else {
            mApp.tts.speak("读卡失败");
            String str = String.valueOf(i);

            IDCard.Lib_IDCardClose();
            return ToolFun.hexStringToBytes(str);
        }
    }
    public byte[] GetICCInfo(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] tagList = new byte[len];
        System.arraycopy(data, ++pos, tagList, 0, len);


        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICCInfo( ictype[0], new String(aidList), new String(tagList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetARQC(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICCArqc( ictype[0], new String(txtdata), new String(aidList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] ARPCExeScript(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] arpc = new byte[len];
        System.arraycopy(data, ++pos, arpc, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] CDol2 = new byte[len];
        System.arraycopy(data, ++pos, CDol2, 0, len);

        String[] str = PBOC.getInstance().ARPCExeScript( ictype[0], new String(txtdata), new String(arpc), new String(CDol2));
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s|%s", str[0], str[1], str[3]).getBytes();
        return arrRet;
    }

    public byte[] GetTrDetail(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] NOLog = new byte[len];
        System.arraycopy(data, ++pos, NOLog, 0, len);


        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetTrDetail( ictype[0], NOLog[0] & 0xff, timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetLoadLog(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] NOLog = new byte[len];
        System.arraycopy(data, ++pos, NOLog, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetLoadLog( ictype[0], NOLog[0] & 0xff, new String(aidList), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s", str[0], str[1]).getBytes();
        return arrRet;
    }

    public byte[] GetICAndARQCInfo(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] ictype = new byte[len];
        System.arraycopy(data, ++pos, ictype, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] aidList = new byte[len];
        System.arraycopy(data, ++pos, aidList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] tagList = new byte[len];
        System.arraycopy(data, ++pos, tagList, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] txtdata = new byte[len];
        System.arraycopy(data, ++pos, txtdata, 0, len);

        pos += len;
        len = data[pos] & 0xff;
        byte[] timeout = new byte[len];
        System.arraycopy(data, ++pos, timeout, 0, len);

        String[] str = PBOC.getInstance().GetICAndARQCInfo( ictype[0], new String(aidList), new String(tagList), new String(txtdata), timeout[0] & 0xff);
        byte[] arrRet = null;
        if (str != null)
            arrRet = String.format("%s|%s|%s", str[0], str[1], str[3]).getBytes();
        return arrRet;
    }
    }
