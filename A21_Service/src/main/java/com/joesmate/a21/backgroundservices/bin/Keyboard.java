package com.joesmate.a21.backgroundservices.bin;

import android.content.Context;
import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.sdk.KeyboardDev;
import com.joesmate.sdk.util.ToolFun;

import java.util.ArrayList;
import java.util.List;

import vpos.apipackage.Pci;

/**
 * Created by andre on 2017/7/25 .
 */

public class Keyboard {
    private static final Keyboard mInstance = new Keyboard();

    int m360fd = -1;
     Context context;

    public void setFD(int fd) {
        m360fd = fd;
    }

    private void Keyboard(Context context) {
        this.context=context;
    }

    public static Keyboard getInstance() {

        return mInstance;
    }

    /**
     * 获取密钥
     *
     * @param data
     * @return
     */
    public byte[]    getPassword(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;
        byte[] iencryType = new byte[len];
        System.arraycopy(data, ++pos, iencryType, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] iTimes = new byte[len];
        System.arraycopy(data, ++pos, iTimes, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] iLength = new byte[len];
        System.arraycopy(data, ++pos, iLength, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] strVoice = new byte[len];
        System.arraycopy(data, ++pos, strVoice, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] EndType = new byte[len];
        System.arraycopy(data, ++pos, EndType, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] strTimeout = new byte[len];
        System.arraycopy(data, ++pos, strTimeout, 0, len);
        // pos += len;

        int enctryTppe = iencryType[0] & 0xff;
        int times = iTimes[0] & 0xff;
        int length = iLength[0] & 0xff;
        String voice = new String(strVoice);
        int endtype = EndType[0] & 0xff;
        int timeout = strTimeout[0] & 0xff;

        App.getInstance().tts.speak(voice);

        byte[] password = new byte[]{};

        byte[] password2 = new byte[]{};
        /**
         * keyNo [in]	PIN密钥索引号，取值范围0~9，必须与认证过程中所使用的AUTHPINK的密钥索引号相同
         min_len[in]	指示输入数据的最小长度，取值范围，4~12。
         max_len[in]	指示输入数据的最大长度，取值范围，4~12。
         card_no[in]	指示相应的卡片或交易序号数据，支持任意长度的卡号输入,不用做任何处理的实际卡号。(以结束符’\x0’结尾）
         mode[in]	加密模式：
         0=X9.8
         1=X3.92(不支持)

         补充：最高位0X80用来指示无PIN时是否可以按确认返回模式：＝0可以确认退出；＝1则不可确认退出(3510暂不支持)。
         pin_block[out]	返回加密的PIN BLOCK
         mark[in]	0: 不带金额
         1: 带金额
         iAmount [in]	金额:
         举例：(123456.00) 如果金额长度小于14，则在后面补0.
         waitTimeSec [in]	等待时间，单位（秒），取值范围为0-６0，
         waittime_sec为0 时表示默认值６0秒；
         当取值大于６0时，为６0秒
         ctx[in]	Context

         * */
        byte[] pinBlock = new byte[8];
        byte[] cardNo = ToolFun.hexStringToBytes("30303030303030303030303030303030");
        byte[] iAmount = ToolFun.hexStringToBytes("3132333435362e30300000000000");
        byte minLen = 6;
        byte maxLen = 6;
        byte waitTimeSec = 60;
        byte mark = 0;
       byte mode = 0;
      int  ret = Pci.Lib_PciGetPin((byte)0, minLen, maxLen, mode, cardNo, pinBlock, mark, iAmount, waitTimeSec, context);//42 9A B0 C2 06 60 D7 95
        if (ret == 0) {
            Log.e("获取pin成功" ,""+ ToolFun.printHexString(pinBlock));
        } else {
            Log.e("获取pin失败", ret +"");
        }
//        if (enctryTppe == 1)
//            password = KeyboardDev.getInstance().getEnablePassword(m360fd, length, endtype, timeout * 1000);
//        else if (enctryTppe == 5)
//            password = KeyboardDev.getInstance().getCryptPassword(m360fd, length, endtype, timeout * 1000);
//
//        List<byte[]> Passwords = new ArrayList<>();
//        while (--times > 0) {
//            App.getInstance().tts.speak("请再次输入密码");
//            if (enctryTppe == 1)
//                password2 = KeyboardDev.getInstance().getEnablePassword(m360fd, length, endtype, timeout * 1000);
//            else if (enctryTppe == 5)
//                password2 = KeyboardDev.getInstance().getCryptPassword(m360fd, length, endtype, timeout * 1000);
//        }

        return pinBlock;
        //  return null;
    }

    /**
     * 初始化密码键盘
     *
     * @return
     */
    public int InitPinPad() {
        return KeyboardDev.getInstance().RestDefaultKey(m360fd);
    }

    /**
     * 密文更新主密钥
     *
     * @param data
     * @return
     */
    public int DownMKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] index = new byte[len];
        System.arraycopy(data, ++pos, index, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zmk = new byte[len];
        System.arraycopy(data, ++pos, Zmk, 0, len);
        int ZmkIndex = index[0], ZmkLength = length[0];

        if (ZmkLength == 8)
            ZmkLength = 8;
        else if (ZmkLength == 16)
            ZmkLength = 16;
        else if (ZmkLength == 32)
            ZmkLength = 32;
        /***
         * byte  key_no[in]	密钥序号,取值范围0~99，其他值非法。
      byte   key_len [in]
         密钥长度,只能取8 、16、24这3个值，其他值非法。
      byte[]   key_data  [in]	密钥数据　
      byte   mode[in]	写入模式
         0x00：直接写入；
         0x01：使用旧的主密钥进行加密；(暂不支持)
         0x81：使用旧的主密钥进行解密；(暂不支持)
         0xff:与旧密钥进行异或后再写入；(暂不支持)
         * */
     int i=   Pci.Lib_PciWritePIN_MKey((byte)ZmkIndex,(byte)ZmkLength,Zmk,(byte)0);
        if(i==0){
            Log.e("主密钥下载成功",i+"");
        }else{
            Log.e("主密钥下载失败",i+"");
        }
        return i;
                //KeyboardDev.getInstance().DownMkey(m360fd, ZmkIndex, ZmkLength, Zmk);
    }

    /**
     * 明文更新主密钥
     *
     * @param data
     * @return
     */
    public int DownMKey2(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] index = new byte[len];
        System.arraycopy(data, ++pos, index, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zmk = new byte[len];
        System.arraycopy(data, ++pos, Zmk, 0, len);
        int ZmkIndex = index[0], ZmkLength = length[0];

        if (ZmkLength == 8)
            ZmkLength = 1;
        else if (ZmkLength == 16)
            ZmkLength = 2;
        else if (ZmkLength == 32)
            ZmkLength = 3;
        return KeyboardDev.getInstance().DownMkey(m360fd, ZmkIndex, ZmkLength, Zmk);
    }

    /**
     * 下载工作密钥
     *
     * @param data
     * @return
     */
    public int DownWKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] Mindex = new byte[len];
        System.arraycopy(data, ++pos, Mindex, 0, len);
        pos += len;

        byte[] Windex = new byte[len];
        System.arraycopy(data, ++pos, Windex, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] length = new byte[len];
        System.arraycopy(data, ++pos, length, 0, len);
        pos += len;

        len = data[pos] & 0xff;
        byte[] Zwk = new byte[len];
        System.arraycopy(data, ++pos, Zwk, 0, len);
        int ZmkIndex = Mindex[0], ZwkIndex = Windex[0], ZmkLength = length[0];
        if (ZmkLength == 8)
            ZmkLength = 8;
        else if (ZmkLength == 16)
            ZmkLength = 16;
        else if (ZmkLength == 32)
            ZmkLength = 32;
        /**
         *  byte key_no[in]	PIN密钥序号，取值范围0~9，其他值非法。
         byte   key_len[in]	密钥长度,只能取8、16、24这3个值，其他值非法。
         byte[]  key_data[in]	密钥数据
         byte   mode[in]	写入模式
         0X00：直接写入
         0X01：利用主密钥进行加密
         0X81：利用主密钥进行解密

         byte   mkey_no[in]	主密钥序号，取值范围0~9，其他值非法。

         * **/
      int i=  Pci.Lib_PciWritePinKey((byte)ZwkIndex,(byte)ZmkLength,Zwk,(byte)81,(byte)ZmkIndex);
        if(i==0){
            Log.e("工作密钥下载成功",i+"");
        }else{
            Log.e("工作密钥下载失败",i+"");
        }
        return i;
        //KeyboardDev.getInstance().DownWkey(m360fd, ZmkIndex, ZwkIndex, ZmkLength, Zwk);
    }


    /**
     * 激活工作密钥
     *
     * @param data
     * @return
     */
    public int ActiveWKey(byte[] data) {
        int pos = 3;
        int len = data[pos] & 0xff;

        byte[] Mindex = new byte[len];
        System.arraycopy(data, ++pos, Mindex, 0, len);
        pos += len;

        byte[] Windex = new byte[len];
        System.arraycopy(data, ++pos, Windex, 0, len);
        int MKeyIndex = Mindex[0], WKeyIndex = Windex[0];
        return KeyboardDev.getInstance().ActiveWKey(m360fd, MKeyIndex, WKeyIndex);
    }
}
