package com.joesmate.a21.backgroundservices.bin;

import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.io.GPIO;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.sdk.util.ToolFun;

import java.util.Arrays;

import vpos.apipackage.Fingerprint;

/**
 * Created by andre on 2017/7/20 .
 */

public class PassThrough {
    private PassThrough() {
    }

    //private static int m_fpfd = App.getInstance().m_fpfd;

    private static byte[] fingerbuffer = new byte[4092];
    private static int[] fingerlen = new int[4092];
    private static final PassThrough mInstance = new PassThrough();
    int iRet;

    public static PassThrough getInstance() {

        return mInstance;
    }

    public int SendCMD(byte[] cmd) {

        iRet = Fingerprint.Lib_FpCommunication(cmd, cmd.length, fingerbuffer, fingerlen, 3000);
        return iRet;
    }

    public byte[] getBuffer(int[] reCode) {

        reCode[0] = fingerlen[0];
        if (fingerlen[0] <= 0) {
            return new byte[]{(byte) 0x30, (byte) 0x11};
        }
        byte[] buff = new byte[fingerlen[0]];
        System.arraycopy(fingerbuffer, 0, buff, 0, fingerlen[0]);
        return buff;
    }

    public int setBaud(int baud) {
        int b = 9600;
        switch (baud) {
            case 0:
                b = 9600;
                break;
            case 1:
                b = 19200;
                break;
            case 2:
                b = 38400;
                break;
            case 3:
                b = 57600;
                break;
            case 4:
                b = 115200;
                break;
        }
        return Fingerprint.Lib_SetFgBaudrate(b);
    }
}
