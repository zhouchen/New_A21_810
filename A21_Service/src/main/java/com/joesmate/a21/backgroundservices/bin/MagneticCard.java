package com.joesmate.a21.backgroundservices.bin;

import android.util.Log;

import com.joesmate.a21.backgroundservices.App;
import com.joesmate.a21.sdk.CMD;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.joesmate.sdk.util.LogMg;
import com.joesmate.sdk.util.ToolFun;

import vpos.apipackage.Mcr;
import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/7/21 .
 */

public class MagneticCard {
    private static MagneticCard mInstance = new MagneticCard();
    byte[]   track1 = new byte[250];
    byte[] track2 = new byte[250];
    byte[] track3 = new byte[250];
    int MagInt;
    private MagneticCard() {

    }

    public static MagneticCard getInstance() {
        return mInstance;
    }



    public int SlotCard() {

     long    startTime=System.currentTimeMillis();
        while (Mcr.Lib_McrCheck() != 0) {
            try {
                if (System.currentTimeMillis() - startTime > 200000) {
                    Mcr.Lib_McrClose();
                 return  -1;
            }
                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

return 0;


    }

    public byte[] ReadData(int[] retcode) {

        byte[] in = new byte[4096];
        int count=0;
        Mcr.Lib_McrRead((byte)0, (byte)0, track1, track2, track3);

        System.arraycopy(track1, 0, in, count,track1.length);
        count+=track1.length;
        System.arraycopy(track2, 0, in, count,track2.length);
        count+=track2.length;
        System.arraycopy(track3, 0, in , count,track3.length);
        count+=track3.length;
        byte[] buffer = new byte[count+3];
        buffer[0] = (byte) track1.length;
        buffer[1] = (byte) track2.length;
        buffer[2] = (byte) track3.length;
        System.arraycopy(in,0,buffer,3,count);
        retcode[0] = count;
        if (count < 0) {
            Mcr.Lib_McrClose();
            return new byte[]{(byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x03};
        }
        if (count== 0) {
            SlotCard();
            Mcr.Lib_McrClose();
            return new byte[]{(byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x03, (byte) 0x03, (byte) 0x03};
        }
        Mcr.Lib_McrClose();
        return buffer;
    }
}
