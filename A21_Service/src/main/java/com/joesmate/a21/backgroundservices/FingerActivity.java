package com.joesmate.a21.backgroundservices;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.joesmate.sdk.util.LogMg;

/**
 * Created by Administrator on 2018-06-22.
 */

public class FingerActivity extends Activity {


        private WebView webView;
        private Intent _intent;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            _intent = getIntent();
            setContentView(R.layout.activity_finger);
            webView = findViewById(R.id.FingerWebView);

            Loadhtml();

            IntentFilter filter = new IntentFilter();
            filter.addAction("action.finger");
            registerReceiver(broadcastReceiver, filter);

        }

        private void Loadhtml() {
            int tag = _intent.getIntExtra("action", 0);
            switch (tag) {
                case 3:
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public boolean shouldOverrideUrlLoading(WebView view, String url) {

                            ShowFinger();   //在2.3上面不加这句话，可以加载出页面，在4.0上面必须要加入，不然出现白屏
                            return true;
                        }
                        //加载https时候，需要加入 下面代码
                        @Override
                        public void onReceivedSslError(WebView view,
                                                       SslErrorHandler handler, SslError error) {

                            handler.proceed();  //接受所有证书
                        }

                    });
                    break;


            }
        }

        private void ShowFinger() {
            webView.loadUrl("file:///android_asset/Finger.html");
        }

        final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int tag = intent.getIntExtra("action", -1);
                switch (tag) {

                    case 4://取消
                        Log.e("指纹显示关闭","");
                      FingerActivity.this.finish();
//                        Message msg = handler.obtainMessage();
//                        msg.what = 4;
//                        msg.obj = intent;
//                        handler.sendMessage(msg);

                        break;


                }
            }
        }

            ;
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {

                        case 4:
                            finish();
                            break;





                    }
                }
            };
    protected void onDestroy() {
        super.onDestroy();
        finish();
        this.unregisterReceiver(broadcastReceiver);
    }
}
