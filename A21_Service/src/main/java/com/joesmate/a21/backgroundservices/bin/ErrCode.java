package com.joesmate.a21.backgroundservices.bin;

public class ErrCode {

    String  errString=null;

    public ErrCode(){

    }
    public static String ErrMsg(int i){
        switch (i){
            case -2403:
                return "通道号错误";

            case -2405:
                return  "卡拔出或无卡";
            case -2404:
                return  "协议错误";
            case -2500:
                return  "IC卡复位的电压模式错误";
            case -2503:
                return  "通信失败";
            case -2401:
                return  "奇偶错误";
            case -2402:
                return  "参数值不能为空";
            case -2406:
                return  "没有初始化";
            case -2100:
                return  "正反向约定(TS)错误";
            case -2101:
                return  "复位校验(TCK)错误";
            case -2102:
                return  "复位等待超时";
            case -2103:
                return  "TA1错误";
            case -2104:
                return  "TA2错误";
            case -2105:
                return  "TB1错误";
            case -2107:
                return  "TB2错误";
            case -2108:
                return  "TB3错误";
            case -2109:
                return  "TC1错误";
            case -2110:
                return  "TC2错误";
            case -2111:
                return  "TC3错误";
            case -2112:
                return  "TD1错误";
            case -2113:
                return  "TD2错误";
            case -2114:
                return  "ATR数据长度错误";
            case -2200:
                return  "等待卡片响应超时";
            case -2201:
                return  "重发错误";
            case -2202:
                return  "重收错误";
            case -2203:
                return  "字符奇偶错误";
            case -2204:
                return  "状态字节无效";
            case -2301:
                return  "字符等待超时(CWT)错误";
            case -2300:
                return  "字组等待超时(BWT)错误  ";
            case -2302:
                return  "异常(ABORT)通信错误";
            case -2303:
                return  "字组校验码(EDC)错误";
            case -2304:
                return  "同步通信错误";
            case -2305:
                return  "字符保护时间(EGT)错误";
            case -2306:
                return  "字组保护时间(BGT)错误";
            case -2307:
                return  "NAD错误";
            case -2308:
                return  "PCB错误";
            case -2309:
                return  "LEN错误";
            case -2310:
                return  "IFSC错误";
            case -2311:
                return  "IFSD错误";
            case -2312:
                return  "重收发错误字组多次错误";
            case -2313:
                return  "字符奇偶错误";
            case -2314:
                return  "无效的字组";


            default:
                return "";
        }

    }
}
