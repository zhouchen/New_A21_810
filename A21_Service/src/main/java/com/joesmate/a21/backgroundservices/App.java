package com.joesmate.a21.backgroundservices;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.IBinder;
import android.util.Log;

import com.jl.pinpad.IRemotePinpad;
import com.joesmate.AndroidTTS.*;
import com.joesmate.BaesTextToSpeech;
import com.joesmate.a21.sdk.ReaderDev;
import com.joesmate.a21.serial_port_api.libserialport_api;
import com.jostmate.IListen.OnReturnListen;
import com.jostmate.btfactory.BtFactory;
import com.jostmate.ibt.BaseBT;
//import com.jollytech.app.Platform;
import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;

import vpos.apipackage.Sys;

/**
 * Created by andre on 2017/7/3 .
 */

public class App extends Application {
    private static final String TAG = "com.joesmate.a21.backgroundservices.App";
    private static App mApp;
//    final String m_360path = "/dev/ttyMT1";//360地址
//    final String m_fppath = "/dev/ttyMT2";//指纹地址
//    final String m_btpath = "/dev/ttyMT3";//蓝牙地址
    public BaseBT btFactory;
    public IRemotePinpad m_pinpad;

    public BaesTextToSpeech tts;
//    //360描述符
//    public int m_360fd = -1;
//    //指纹描述符
//    public int m_fpfd = -1;
    //蓝牙描述符
    public int m_btfd = -1;

//    final File m_360file = new File(m_360path);
//    final File m_fpfile = new File(m_fppath);
//    final File m_btfile = new File(m_btpath);


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Application=", "<====>App_onCreate<====>");
        mApp = this;
        tts = new TextToSpeechEx(this.getApplicationContext());
//        ReaderDev.getInstance().FpPowerOn();
        openBt();//打开蓝牙

        connectPinpad();
    }

    void connectPinpad() {

        Intent service = new Intent("com.remote.service.PINPAD");
        Intent eintent = new Intent(getExplicitIntent(mApp.getApplicationContext(), service));
        bindService(eintent, connection, Context.BIND_AUTO_CREATE);
    }

    ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // TODO Auto-generated method stub
//            Tools.Loginfo(getClass().getName(), "pinpad disconnected!");
            m_pinpad = null;
        }

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            // TODO Auto-generated method stub
//            Tools.Loginfo(getClass().getName(), "pinpad connected!!!!");
            m_pinpad = IRemotePinpad.Stub.asInterface(arg1);
            if (m_pinpad != null) {
                Log.e("密码键盘服务开启", "121212");
            }
        }
    };

    public static App getInstance() {

        return mApp;
    }


    private OnReturnListen mlisten = null;

    public void setOnReturnListen(OnReturnListen listen) {
        mlisten = listen;
    }

    public OnReturnListen getOnReturnListen() {
        if (mlisten != null)
            return mlisten;
        return null;
    }

    public static Intent getExplicitIntent(Context context,
                                           Intent implicitIntent) {
        // Retrieve all services that can match the given intent
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfo = pm.queryIntentServices(implicitIntent,
                0);
        // Make sure only one match was found
        if (resolveInfo == null || resolveInfo.size() != 1) {
            return null;
        }
        // Get component info and create ComponentName
        ResolveInfo serviceInfo = resolveInfo.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        // Create a new intent. Use the old one for extras and such reuse
        Intent explicitIntent = new Intent(implicitIntent);
        // Set the component to be explicit
        explicitIntent.setComponent(component);
        return explicitIntent;
    }

    public void openBt() {
        try {
            btFactory = BtFactory.CreateBT(this.getApplicationContext());
            btFactory.openBt();

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public BaseBT getBt() {

        return btFactory;
    }

}
