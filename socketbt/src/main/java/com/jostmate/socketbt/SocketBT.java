package com.jostmate.socketbt;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import com.jostmate.ibt.BaseBT;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * @author andrewliu
 * @create 2018/7/18
 * @Describe
 */
public class SocketBT implements BaseBT {
    public static final String PROTOCOL_SCHEME_L2CAP = "btl2cap";
    public static final String PROTOCOL_SCHEME_RFCOMM = "btspp";
    public static final String PROTOCOL_SCHEME_BT_OBEX = "btgoep";
    public static final String PROTOCOL_SCHEME_TCP_OBEX = "tcpobex";
    private static boolean mConneted = false;
    private static boolean mClosed = true;
    private Context mContext;
    ServerThread mServerThread = null;
    private static BluetoothServerSocket mServerSocket = null;//蓝牙服务端socket
    private static BluetoothSocket mSocket = null;// 蓝牙客户端socket

    private static BluetoothAdapter mBluetoothAdapter = null;//蓝牙适配器


    InputStream in;
    OutputStream out;

    public SocketBT(Context context) throws Exception {
        mContext = context;
        iniBtAdapter();
        // iniBtServerSocket();
    }

    private void iniBtAdapter() throws Exception {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            throw new Exception("蓝牙未找到");
        }
        //mBluetoothAdapter.disable();
        if (!mBluetoothAdapter.isEnabled()) {
            Log.w("iniBtAdapter", "mBluetoothAdapter.isEnabled()=fales");
            mBluetoothAdapter.enable();
        }
    }

    private static void iniBtServerSocket() throws Exception {


        try {

//            mServerSocket = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(PROTOCOL_SCHEME_RFCOMM, UUID.fromString("00001102-0000-1000-8000-00805F9B34FB"));
            UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            mServerSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("A21 SPP", MY_UUID);

//            Method listenMethod = mBluetoothAdapter.getClass().getMethod("listenUsingRfcommOn", new Class[]{int.class});
//            mServerSocket = (BluetoothServerSocket) listenMethod.invoke(mBluetoothAdapter, new Object[]{29});


        } catch (SecurityException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        } catch (IllegalArgumentException e) {

            // TODO Auto-generated catch block

            e.printStackTrace();

        }
//        catch (NoSuchMethodException e) {
//
//            // TODO Auto-generated catch block
//
//            e.printStackTrace();
//
//        } catch (IllegalAccessException e) {
//
//            // TODO Auto-generated catch block
//
//            e.printStackTrace();
//
//        } catch (InvocationTargetException e) {
//
//            // TODO Auto-generated catch block
//
//            e.printStackTrace();
//
//        }

    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public int openBt() {

        mClosed = false;
        if (mServerThread == null)//双判断加锁，保证线程安全
            synchronized (this) {
                if (mServerThread == null) {
                    mServerThread = new ServerThread();
                }

            }
        if (!mServerThread.isAlive()) {
            mServerThread.start();
        }
        return 0;
    }

    @Override
    public int closeBt() {
        mClosed = true;
        new Thread() {
            public void run() {
                try {
                    if (mSocket != null) {
                        mSocket.close();
                        mSocket = null;
                    }
                    if (mServerSocket != null) {
                        mServerSocket.close();
                        mServerSocket = null;
                        mConneted = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (mServerThread != null) {
                    mServerThread.interrupt();
                    mServerThread = null;
                }
            }
        }.start();
        return 0;
    }

    @Override
    public int readBt(byte[] buffer) throws Exception {
        if (!mConneted || mServerSocket == null || mSocket == null || !mSocket.isConnected() || in == null)//未连接
            return -1;
        int iRet = -1;
        try {
            iRet = in.read(buffer);
        } catch (IOException ex) {
            in.close();
            out.close();
            mSocket.close();
            mConneted = false;
            throw ex;
        }

        return iRet;
    }

    @Override
    public int writeBt(byte[] buffer, int length) throws Exception {
        out.flush();
        out.write(buffer, 0, length);
        return length;
    }

    @Override
    public boolean getIsConneted() throws Exception {
        return mConneted;

    }

    class ServerThread extends Thread {
        public ServerThread() {
            try {
                iniBtServerSocket();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void run() {
            while (true) {
                if (mClosed)
                    break;
                /* 接受客户端的连接请求 */
                try {
                    if (mServerSocket == null) {
                        iniBtServerSocket();
                    }
                    mSocket = mServerSocket.accept();
                    if (mSocket.isConnected()) {

                        in = mSocket.getInputStream();
                        out = mSocket.getOutputStream();
                        mConneted = true;
                    }

                } catch (Exception ex) {
                    mConneted = false;
                    ex.printStackTrace();
                    //  SocketBT.this.iniBtServerSocket();
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
