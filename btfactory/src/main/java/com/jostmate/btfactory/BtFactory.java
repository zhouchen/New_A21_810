package com.jostmate.btfactory;

import android.content.Context;
import android.util.Log;

import com.jostmate.ibt.BaseBT;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Properties;

/**
 * @author andrewliu
 * @create 2018/7/18
 * @Describe
 */
public class BtFactory {
    public static BaseBT CreateBT(Context context) throws Exception {
        try {
            Properties properties = new Properties();
            InputStream _in = context.getAssets().open("Bt.config");//读取配置文件
            properties.load(_in);
            String classpath = properties.getProperty("btconnet", "");//获取类
            Class CClass = Class.forName(classpath);
            Constructor c = CClass.getConstructor(Context.class);
            Object obj = c.newInstance(context);
            if (obj == null) {
                throw new Exception(String.format("%s类不存在", classpath));
            }
            return (BaseBT) obj;
        } catch (Exception ex) {
            Log.e("BtFactory", "CreateBT", ex);
            throw ex;
        }

    }
}
